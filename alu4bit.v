`timescale 1ns / 1ps

module ALU4Bit(a, b, cin, less, op, result, cout, set, zero, G, P);
input [3:0] a, b;
input cin, less;
input [2:0] op; // op[2] is "binv". op[1:0] denotes the 2-bit operation.

output [3:0] result;
output cout, set, zero, G, P;


wire c0, c1, c2, c3,c0_result,zer3, g, p,g0, g1, g2, g3, p0, p1, p2, p3,set0, set1, zer1,set2, set3,co0, co1, co2, co3,zer0,zer2,ls,ls1, ls2,ls3;


ALU1Bit alu1(a[0], b[0], c0, ls, op[2:0], result[0], co0, set0, zer0, g0, p0);
ALU1Bit alu2(a[1], b[1], c1, ls1, op[2:0], result[1], co1, set1, zer1, g1, p1);
ALU1Bit alu3(a[2], b[2], c2, ls2, op[2:0], result[2], co2, set2, zer2, g2, p2);
ALU1Bit alu4(a[3], b[3], c3, ls3, op[2:0], result[3], co3, set3, zer3, g3, p3);
CLA cla(g0, p0, g1, p1, g2, p2, g3, p3, cin, c0, c1,c2, c3, co_result, g, p);

assign G = g;
assign cout = cout_result;
assign P = p;
assign set = set3;
//assign zero = zero0 & zero1 & zero2 & zero3;
assign zero = ~zer0 & ~zer1 & ~zer2 & ~zer3;


endmodule
