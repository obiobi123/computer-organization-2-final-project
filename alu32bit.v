
module ALU32Bit(a, b, op, result, zero);
input [31:0] a, b;
input [2:0] op; // op[2] is "binv". op[1:0] denotes the 2-bit operation.
output [31:0] result;
output zero; 
wire c0,set0, set1,z0, z1,co0, co1,ls,ls1;

ALU16Bit alu1(a[15:0], b[15:0], c0, ls, op[2:0], result[15:0], co0, set0, zero0, g0, p0);
ALU16Bit alu2(a[31:16], b[31:16], cout0, less1, op[2:0], result[31:16], co1, set1, zero1, g1, p1);
assign zero = ~z0 & ~z1;
endmodule

