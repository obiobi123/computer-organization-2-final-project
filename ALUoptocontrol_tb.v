module ALUOpToALUControl_tb();
  reg [1:0] ALUOpcode;
  reg [5:0] Function;
  wire [2:0] ALUControl;

  ALUOpcodeToALUControl a(ALUOpcode, Function, ALUControl);

  initial begin
    $monitor("  ALUOpcode:%b  Function:%b  ALUControl:%b", ALUOpcode, Function, ALUControl);

    $display("Test  for Load and Store");
    ALUOpcode = 'd 0;
    #1;

    $display("Test for Branch)");
    ALUOpcode = 'd 1;
    #1;

    $display("Test for ADD");
    ALUOpcode = 'd 2;
    Function = 'd 32;
    #1;

    $display("Test for SUB");
    ALUOpcode = 'd 2;
    Function = 'd 34;
    #1;

    $display("Test for AND");
    ALUOpcode = 'd 2;
    Funct = 'd 36;
    #1;

    $display("Test for OR");
    ALUOpcode = 'd 2;
    Function = 'd 37;
    #1;

    $display("Test for SLT");
    ALUOpcode = 'd 2;
    Funct = 'd 42;
    #1;

    $finish;
  end
endmodule