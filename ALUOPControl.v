module ALUOpToALUControl(ALUOp, Funct, ALUControl);
  input [1:0] ALUOp;
  input [5:0] Funct;
  output reg [2:0] ALUControl;

  always @(*) begin
    case (ALUOp)
    'd 0: ALUControl <= 'd 2;
    'd 1: ALUControl <= 'd 3;
    'd 2:
      case (Funct)
      'd 32: ALUControl <= 'd 2;
      
      'd 34: ALUControl <= 'd 3;
      
      'd 36: ALUControl <= 'd 0;
      
      'd 37: ALUControl <= 'd 1;
  
      'd 42: ALUControl <= 'd 7;
      endcase
    endcase
  end
endmodule