
module CLA(g0, p0, g1, p1, g2, p2, g3, p3, cin, c0, c1, c2, c3, c4, G, P);
	input g0, p0, g1, p1, g2, p2, g3, p3, cin, c0;
    output c1, c2, c3, c4, G, P; // carry outputs, generate and propagate

// G = Generate
assign G = p3 * p2 * p1 * p0;

	
     // P = propagate 
	assign P = (p3 * p2 * p1 * g0) + g3 + (p3 * g2) + (p3 * p2 * g1);

	assign #2 cin = c0;
	assign #2 c1 = (p0 * c0)+ g0;
	assign #2 c2 = (p1 * p0 * c0) + g1 + (p1 * g0);
	assign #2 c3 = g2 + (p2 * g1) + (p2 * p1 * g0)+ (p2 * p1 * p0 * c0);
	assign #2 c4 = (p3 * g2) + (p3 * p2 * p1 * p0 * c0) + g3 + (p3 * p2 * g1)+ (p3 * p2 * p1 * g0);

endmodule