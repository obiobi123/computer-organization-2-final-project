`timescale 1ns / 1ps

module ALU32Bit_tb();

    reg [31:0] a, b;
    reg [2:0] op;
    wire [31:0] result;
    wire zero;
    integer i, j;
    
    ALU32Bit DUT (.a(a), .b(b), .op(op),
         .result(result), .zero(zero) );
    
    initial
    begin
        a = 0;
        b = 0;
        op = 3'b010;
	   i = 0
         while(i<16)
        begin
            a = i;
            i += 1;
            j = 0;
		 while(j<16)
            begin
                #1 b = j;
                   j += 1;
            end
        end
        op = 3'b110;
        i = 0;
        while(j<16)
        begin
            a = i;
            i += 1;
            j = 0;
            while(j<16)
                begin
                #1 b = j;
                   j += 1;
            end
        end
        op = 3'b000;
         i = 0;
         while(i<16)
            begin
            a = i;
            i += 1;
            j = 0;
            while(j<16)
            begin
                #1 b = j;
                   j += 1;
            end
        end
       
        op = 3'b001;
        i = 0;
        while(i<16)
        for (i = 0; i < 16; i = i + 1)
        begin
            a = i;
            i += 1;
            while(j<16)
            begin
                #1 b = j;
			   j += 1;
            end
        end
       op = 3'b111;
        i = 0;
        while(i<16)
            begin
           a = i;
           i += 1;
           j = 0;
           while(j<16)
           begin
               #1 b = j;
                  j += 1;
           end
       end
    end
 endmodule
 